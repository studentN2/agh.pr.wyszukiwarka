/* 
 * File:   main.cpp
 * Author: studentN2
 *
 * Created on 23 maj 2013, 17:45
 * 
 * reczne kompilowanie programu (po zainstalowaniu openMPI):
 * mpiCC main.cpp Rec.cpp EngineArray.cpp EngineSort.cpp EngineRec.cpp Parser.cpp Timer.cpp -o main && mpirun -np 1 ./main
 */
using namespace std;

#include <cstdlib>
#include <iostream>
#include <vector>


#include "mpi.h"

#include "Timer.h"
#include "Rec.h"
#include "Parser.h"
#include "EngineArray.h"
#include "EngineSort.h"
#include "EngineRec.h"

/*
 * propozycja zrobienia tego programu (jesli ktos bedzie mial poprawki - smialo pisac ;) )
 * 
 * main.cpp - plik główny -wspólny dla wszystkich więc on będzie bez zmian
 * 
 * =======
 * 
 * main.cpp - inicjacja MPI + przypisanie ID do procesów
 * 
 * każda klasa musi mieć funkcje np. "void run()", która bedzie obsługiwać daną klasę, 
 * tj. odbierać wiadomości, uruchamiać algorytmy danej klasy i wysyłać wyniki do dalszego procesu
 * 
 * obecny program działa na 5 procesach (ID od 0 do 4), gdzie ID=0 to proces główny.
 * 
 * na chwile obecna ID=0 wysyła liczbe do procesu ID=2. 2 odbiera wiadomość i wyświetla liczbe na ekran ;)
 * 
 */
#define DESC  1
#define ASC  0

int main(int argc, char** argv) {
	
	/* open MPI */
  MPI::Init ( argc, argv );
  int id = MPI::COMM_WORLD.Get_rank ( ); // ID procesu
  int p_all = MPI::COMM_WORLD.Get_size ( ); // liczba wszystkich procesow
  
	//----------------------------------
	

    vector< vector<int> > MACIERZ_IDS;
    vector<int> IDS;
    vector<Rec> RECS;
    
  cout << "Aktualnie pracuje proces: " << id << ", wszystkich: " << p_all << endl;
  
		Parser p;                           // Mateusz
		EngineArray eArray; 			// Krzysiek
		EngineRec eRec;                  // Monika
		EngineSort eSort;               // Sylwia
		
  switch (id) {//id procesu
	  case 1:
		//r.run();
	  break;//1
	  case 2:
		eArray.run();
	  break; // 2
	  case 3:
		//eRec.run();
	  break; // 3
	  case 4:
		//eSort.run();
	  break; // 4
	  default: // proces 0 - main
		
    // ===== generuj przykladowa tabele..
    int M_IDS_I = 2;  int M_IDS_J = 2; // wymiary
    
    int **M_IDS = new int *[ M_IDS_I ];
    for (int i=0;i<M_IDS_I;i++) M_IDS[i] = new int [M_IDS_J];
    
    M_IDS[0][0] = M_IDS[0][1] = 2;
    M_IDS[1][0] = 3;
    M_IDS[1][1] = 4;
    // ======= ---
		
		 // ===== wysylaj tabele M_IDS do procesu 2
		int i_buffer[] = {M_IDS_I, M_IDS_J};
    MPI::COMM_WORLD.Send ( i_buffer, 2, MPI::INT, 2, 1 ); // tabela, rozmiar, typ, adresat, tag
    
		for (unsigned int i=0;i<M_IDS_I;i++) {//i
			for (unsigned int j=0;j<M_IDS_J;j++) {//j
				 MPI::COMM_WORLD.Send ( M_IDS[i], M_IDS_J, MPI::INT, 2, 2 );
			}//jj
		}//i
    // ======= ---
		
		 // =====  czekaj na wynik z procesu 2
	int r_buffer[2];
	MPI::COMM_WORLD.Recv ( r_buffer, 2, MPI::INT, 2, 3 ); // zmianna, rozmiar, typ, nadawca, tag, status
	
	if (r_buffer[1] != 0) { cout << "dopuszczalna tabela tylko 1 wymiarowa!"; return 1; }
	
	// odczyt liczb
	int *n_buff = new int[ r_buffer[0] ];
	MPI::COMM_WORLD.Recv ( n_buff, r_buffer[0], MPI::INT, 2, 3);
	
	// wyswietl IDS
	cout << "Proces "<<id<<" wyswietla IDS:" <<endl;
		for (unsigned int i=0;i<r_buffer[0];i++) {
			cout << "IDS["<<i<<"] = " << n_buff[i] << endl;
		}
		cout << "\n\n";
    // ======= ---
		
	  break;//0
  }//id procesu
    
    

/*

    //    MACIERZ_IDS = p.loadFile("dane_wejsciowe.txt");
    IDS = eArray.arrayUnique(MACIERZ_IDS);
    //RECS = eRec.generateRec(IDS);
    RECS = eSort.sort(RECS, "ocena", DESC); 

    //    cout << p->display(RECS) << endl;
    cout << "IDS:" << endl;
    for (unsigned int i=0;i<IDS.size();i++) {
        cout << IDS[i] << " ";
    }
    cout << "\n\n";
*/

    // koniec MPI
    MPI::Finalize ( );
    
    return 0;
}

